/**
 * 
 */
package textgen;

import static org.junit.Assert.*;

import java.util.LinkedList;

import org.junit.Before;
import org.junit.Test;

/**
 * @author UC San Diego MOOC team
 *
 */
public class MyLinkedListTester {

	private static final int LONG_LIST_LENGTH =10; 

	MyLinkedList<String> shortList;
	MyLinkedList<Integer> emptyList;
	MyLinkedList<Integer> longerList;
	MyLinkedList<Integer> list1;
	
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		// Feel free to use these lists, or add your own
	    shortList = new MyLinkedList<String>();
		shortList.add("A");
		shortList.add("B");
		emptyList = new MyLinkedList<Integer>();
		longerList = new MyLinkedList<Integer>();
		for (int i = 0; i < LONG_LIST_LENGTH; i++)
		{
			longerList.add(i);
		}
		list1 = new MyLinkedList<Integer>();
		list1.add(65);
		list1.add(21);
		list1.add(42);
		
	}

	
	/** Test if the get method is working correctly.
	 */
	/*You should not need to add much to this method.
	 * We provide it as an example of a thorough test. */
	@Test
	public void testGet()
	{
		//test empty list, get should throw an exception
		try {
			emptyList.get(0);
			fail("Check out of bounds");
		}
		catch (IndexOutOfBoundsException e) {
			
		}
		
		// test short list, first contents, then out of bounds
		assertEquals("Check first", "A", shortList.get(0));
		assertEquals("Check second", "B", shortList.get(1));
		
		try {
			shortList.get(-1);
			fail("Check out of bounds");
		}
		catch (IndexOutOfBoundsException e) {
		
		}
		try {
			shortList.get(2);
			fail("Check out of bounds");
		}
		catch (IndexOutOfBoundsException e) {
		
		}
		// test longer list contents
		for(int i = 0; i<LONG_LIST_LENGTH; i++ ) {
			assertEquals("Check "+i+ " element", (Integer)i, longerList.get(i));
		}
		
		// test off the end of the longer array
		try {
			longerList.get(-1);
			fail("Check out of bounds");
		}
		catch (IndexOutOfBoundsException e) {
		
		}
		try {
			longerList.get(LONG_LIST_LENGTH);
			fail("Check out of bounds");
		}
		catch (IndexOutOfBoundsException e) {
		}
		
	}
	
	
	/** Test removing an element from the list.
	 * We've included the example from the concept challenge.
	 * You will want to add more tests.  */
	@Test
	public void testRemove()
	{
		int a = list1.remove(0);
		assertEquals("Remove: check a is correct ", 65, a);
		assertEquals("Remove: check element 0 is correct ", (Integer)21, list1.get(0));
		assertEquals("Remove: check size is correct ", 2, list1.size());
		
		// TODO: Add more tests here
		assertEquals("Remove: check the sentile head", list1.head.next.data, list1.get(0));
		
		try {
			emptyList.remove(-1);
			fail("check out bound");
		} catch (IndexOutOfBoundsException e) {
			
		}
		
		try {
			list1.remove(5);
			fail("check out bound");
		} catch (IndexOutOfBoundsException e) {
			
		}
		
	}
	
	/** Test adding an element into the end of the list, specifically
	 *  public boolean add(E element)
	 * */
	@Test
	public void testAddEnd()
	{
		// add to an empty list
		boolean results=list1.add((Integer)80);
		if (results) {
			assertEquals("Add: cheak last element if addition succeeds.",(Integer)80,list1.get(3));		
			assertEquals("Add: cheak previous element of the last elment if addition succeeds.",(Integer)42,list1.get(2));
			assertEquals("Add: check size is correct ",4,list1.size());
		}
		else {
			assertEquals("Add: cheak previous element of the last elment if addition succeeds.",(Integer)21,list1.get(1));
			assertEquals("Add: cheak previous element of the last elment if addition succeeds.",(Integer)42,list1.get(2));
			assertEquals("Add: check size is correct ", 3, list1.size());
		}
		
		
		try {
			list1.add(null);
			fail("check out of bound");
		} catch (NullPointerException e) {
			
		}
		
	}

	
	/** Test the size of the list */
	@Test
	public void testSize()
	{
		// TODO: implement this test
		assertEquals("Size: empty size", 0, emptyList.size());
		
		emptyList.add(3);
		assertEquals("Size: add one element", 1, emptyList.size());
		
		emptyList.remove(0);
		assertEquals("Size: remove one element", 0, emptyList.size());
	}

	
	
	/** Test adding an element into the list at a specified index,
	 * specifically:
	 * public void add(int index, E element)
	 * */
	@Test
	public void testAddAtIndex()
	{
		list1.add(0, 88);
		assertEquals("Add: check the value", (Integer)88, list1.get(0));
		assertEquals("Add: add at head", (Integer)65, list1.get(1));
		assertEquals("Add: check size is correct ", 4, list1.size());
		assertEquals("Add: add at head", (Integer)88, list1.head.next.data);
		
		list1.add(3, 11);
		assertEquals("Add: check the value", (Integer)11, list1.get(3));
		assertEquals("Add: add at head", (Integer)21, list1.get(2));
		assertEquals("Add: add at head", (Integer)42, list1.get(4));
		assertEquals("Add: check size is correct ", 5, list1.size());
		assertEquals("Add: add at head", (Integer)42,list1.tail.prev.data);
		
		list1.add(1, 22);
		assertEquals("Add: check the ", (Integer)22, list1.get(1));
		assertEquals("Add: add at head", (Integer)88, list1.get(0));
		assertEquals("Add: add at head", (Integer)65, list1.get(2));
		assertEquals("Add: check size is correct ", 6, list1.size());
		
		try {
			list1.add(-1, 33);
			fail("check out of bound");
		} catch (IndexOutOfBoundsException e) {
			
		}
		
		try {
			list1.add(10, 88);
			fail("check out of bound");
		} catch (IndexOutOfBoundsException e) {
			
		}
		
		
	}
	
	/** Test setting an element in the list */
	@Test
	public void testSet()
	{
		int a = list1.set(0, 11);
		assertEquals("Add: check the ", (Integer)11, list1.get(0));
		assertEquals("Add: add at head", 65, a);
		
		int b = list1.set(2, 22);
		assertEquals("Add: add at head", (Integer)22, list1.get(2));
		assertEquals("Add: check size is correct ", 42, b);
		
		int c = list1.set(1, 33);
		assertEquals("Add: add at head", (Integer)33, list1.get(1));
		assertEquals("Add: check size is correct ", 21, c);
		assertEquals("Add: check size is correct ", (Integer)11, list1.get(0));
		assertEquals("Add: check size is correct ", (Integer)22, list1.get(2));
		
		
		try {
			list1.set(-1, 0);
			fail("check out bound");
		} catch (IndexOutOfBoundsException e) {
			
		}
		
		try {
			list1.set(4, 0);
			fail("check out bound");
		} catch (IndexOutOfBoundsException e) {
			
		}
		
		try {
			list1.set(2, null);
			fail("check out the element");
		} catch (NullPointerException e) {
			
		}
	    
	}
	
	
	
}
