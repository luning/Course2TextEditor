package textgen;

import static org.junit.Assert.*;

import java.util.Random;

import org.junit.Before;
import org.junit.Test;

public class MarkovTextGeneratorTester {
	
	private MarkovTextGeneratorLoL generator;
	private String sourceText;
	
	@Before
	public void setUp() {
		generator = new MarkovTextGeneratorLoL(new Random(22));
		sourceText = "hi there hi Leo";
	}
	
	@Test
	public void trainTest() {
		
		generator.train(sourceText);
		
		assertEquals("hi: there->Leo->\nthere: hi->\nLeo: hi->\n",generator.toString());
	}

}
